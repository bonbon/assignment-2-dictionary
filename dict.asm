%include "lib.inc"

%define POINTER_SIZE 8 ; Since we use dq, it uses 64-bit quadword

section .text

; node:
;   dq previous_node
;   db "key", 0
;   db "value", 0

; rdi - Null-terminated key string
; rsi - Starting dictionary node
; Returns a pointer to the corresponding dictionary node/entry
global find_word
find_word:
  push r12
  push r13
  .local_rec:
    mov r12, rdi
    mov r13, rsi
    add rsi, POINTER_SIZE
    call string_equals
    test rax, rax
    jz .try_next
    mov rax, r13
    pop r13
    pop r12
    ret
    .try_next:
      mov rdi, r12
      mov rsi, r13
      mov rsi, [rsi]
      test rsi, rsi
      jnz .local_rec
      xor rax, rax
      pop r13
      pop r12
      ret 

; rdi - Pointer to the dictionary node/entry
; Returns a pointer to the value-string
global get_entry_value
get_entry_value:
  add rdi, POINTER_SIZE
  push rdi
  call string_length
  pop rdi
  add rax, rdi
  inc rax
  ret
