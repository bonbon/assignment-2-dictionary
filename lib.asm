%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0

%define STDERR 2
%define STDOUT 1
%define STDIN 0

section .text

; Принимает код возврата и завершает текущий процесс
global exit
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    mov rcx, rdi
    .loop:
        mov dl, byte[rcx]
        inc rcx
        test dl, dl
        jnz .loop
    sub rcx, rdi
    dec rcx
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    push rdi
    call string_length
    pop rsi            ; pointer
    mov rdx, rax       ; length
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    ret

global print_string_err
print_string_err:
  push rdi
  call string_length
  pop rsi
  mov rdx, rax
  mov rax, SYS_WRITE
  mov rdi, STDERR
  syscall
  ret

; Переводит строку (выводит символ с кодом 0xA)
global print_char
global print_newline
print_newline:
    mov rdi, `\n`
    ; Принимает код символа и выводит его в stdout
    print_char:
        push di
        mov rdx, 1   ; length
        mov rax, SYS_WRITE
        mov rsi, rsp ; pointer
        mov rdi, STDOUT
        syscall
        pop di
        ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.


; Выводит знаковое 8-байтовое число в десятичном формате
global print_int
global print_uint
print_int:
    and rdi, rdi
    jns print_uint
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    ; Выводит беззнаковое 8-байтовое число в десятичном формате
    print_uint:
        mov rsi, rsp ; saving the current stack pointer
        dec rsp      ; push the null-terminator
        mov byte[rsp], 0
        mov rax, rdi
        .loop:
            mov rdi, 10  ; setting the divider
            xor rdx, rdx ; zeroing out the  for div operation
            div rdi
            add rdx, '0' ; converting a digit to ascii char-code
            dec rsp      ; pushing one-byte character
            mov byte[rsp], dl
            test rax, rax
            jnz .loop
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi
        mov rsp, rsi
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    push rdi ; saving string pointers for later use
    push rsi
    call string_length ; computing the lengths of the strings
    mov rdi, [rsp]
    push rax
    call string_length
    pop rdx ; rax <- length 1, rdx <- length 2
    pop rsi ; rsi <- string 2, rdi <- string 1
    pop rdi
    cmp rax, rdx
    jne .noteq
    test rax, rax ; testing for 0 length string
    jz .eq
    mov rcx, rax
    .loop:
        mov al, byte[rdi]
        cmp al, byte[rsi]
        jne .noteq
        inc rdi
        inc rsi
        loop .loop
    .eq:
        mov rax, 1
        ret
    .noteq:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push rax
    mov rax, SYS_READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .eof
    pop rax
    ret
    .eof:
        pop rax
        xor rax, rax
        ret

; rdi - Pointer to the buffer to read to
; rsi - Buffer size
; Reads from stdin to the buffer, until \0 or \n is encountered
; Read string is automatically null-terminated
; Returns: rax - buffer address (0 on fail)
;          rdx - string length
global read_line
read_line:
  push r12
  push r13
  push r14
  mov r12, rdi
  mov r13, rsi
  mov r14, 1
  test r13, r13
  jz .overflow
  .read_loop:
    call read_char
    mov byte[r12], al
    test al, al
    jz .return
    cmp al, `\n`
    jz .return
    cmp r14, r13
    jae .overflow
    inc r14
    inc r12
    jmp .read_loop
  .return:
    mov byte[r12], 0
    mov rdx, r14
    dec rdx
    mov rax, r12
    sub rax, rdx
    pop r14
    pop r13
    pop r12
    ret
  .overflow:
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    .trim_loop:
        call read_char
        cmp rax, ' '
        je .trim_loop
        cmp rax, `\t`
        je .trim_loop
        cmp rax, `\n`
        je .trim_loop
    mov r14, 1
    .read_loop:
        cmp r13, r14
        jbe .error
        mov byte[r12], al
        test rax, rax
        jz .return
        cmp rax, ' '
        jz .return
        cmp rax, `\t`
        jz .return
        cmp rax, `\t`
        jz .return
        xor rax, rax
        call read_char
        inc r12
        inc r14
        jmp .read_loop
    .return:
    mov byte[r12], 0
    dec r14
    sub r12, r14
    mov rax, r12
    mov rdx, r14
    pop r14
    pop r13
    pop r12
    ret
    .error:
        pop r14
        pop r13
        pop r12
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    mov rcx, rdi
    xor rsi, rsi
    xor rdx, rdx
    xor rax, rax
    mov rdi, 10
    .loop:
        mov sil, byte[rcx]
        cmp rsi, 48
        jb .return
        cmp rsi, 57
        ja .return
        test rdx, rdx
        push rdx
        jz .after_shift
            mul rdi
        .after_shift:
        sub rsi, 48
        add rax, rsi
        pop rdx
        inc rdx
        inc rcx
        jmp .loop
    .return:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    mov al, byte[rdi]
    cmp al, '-'
    je .parse_signed
        jmp parse_uint
    .parse_signed:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .return
        inc rdx
        neg rax
        .return:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        je .error
        mov al, byte[rdi]
        mov byte[rsi], al
        inc rdi
        inc rsi
        inc rcx
        test rax, rax
        jnz .loop
    mov rax, rcx
    ret
    .error:
        xor rax, rax
        ret

