section .rodata

colon "nasm", entry_nasm
db "the Netwide Assembler, a portable 80x86 assembler", 0

colon "clang", entry_clang
db "the Clang C, C++, and Objective-C compiler", 0

colon "awk", entry_awk
db "pattern-directed scanning and processing language", 0

%define DICTIONARY entry_awk

