%define prev_ident 0
%macro colon 2
  %ifnstr %1
    %error "First argument to the 'colon' macro can only be a string"
  %endif
  %ifnid %2
    %error "Second argument to the 'colon' macro can only be an identifier"
  %endif
  %2:
    dq prev_ident
    db %1, 0
  %define prev_ident %2
%endmacro

