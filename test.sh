#!/usr/bin/bash

test_expect()
{
  if [[ "$2" == "$3" ]]; then
    echo "Test $1 passed"
  else
    echo "Test $1 failed"
    echo "Expected '$3', but got '$2'"
  fi
}

output=`echo "awk" | ./main.out 2> /dev/null`
expected="pattern-directed scanning and processing language"

test_expect "#1" "$output" "$expected"

output=`echo "clang" | ./main.out 2> /dev/null`
expected="the Clang C, C++, and Objective-C compiler"

test_expect "#2" "$output" "$expected"

output=`echo "nasm" | ./main.out 2> /dev/null`
expected="the Netwide Assembler, a portable 80x86 assembler"

test_expect "#3" "$output" "$expected"

output=`echo "non-existant" | ./main.out 2>&1 > /dev/null`
expected="Selected key wasn't found"

test_expect "#4" "$output" "$expected"

